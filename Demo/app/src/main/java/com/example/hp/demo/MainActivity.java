package com.example.hp.demo;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import com.example.hp.demo.util.SerialPortUtils;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        Button button1 = (Button) findViewById(R.id.button);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                byte[] sendMsg = {
                        0x55, (byte) 0xAA,   //起始位
                        0x00, 0x04,          //包长度（从设备厂商标识到数据区字节数）
                        0x00, 0x00,          //设备厂商标识（固定）
                        0x00, 0x00,          //命令字
                        0x04,                //校验位
                        0x55, (byte) 0xAA    //结束位
                };
                SerialPortUtils serialPort = new SerialPortUtils("/dev/ttyO5",115200);
                serialPort.open();
                serialPort.setOnDataReceiveListener(new SerialPortReceive());
                serialPort.sendByteArr(sendMsg);
                serialPort.close();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class SerialPortReceive implements SerialPortUtils.OnDataReceiveListener {
        @Override
        public void onDataReceive(byte[] buffer, int size) {
            Log.d("收到的数据：", "" + Arrays.toString(buffer));
            Log.d("数据的长度：", "" + size);
        }
    }
}
